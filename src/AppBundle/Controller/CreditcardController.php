<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Form\CieloCreditcardForm;
use AppBundle\Service\Cielo;
use GuzzleHttp\Exception\ClientException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CreditcardController
 * @package AppBundle\Controller
 * @Route("/creditcard")
 */
class CreditcardController extends Controller
{
    /**
     * @Route("/", name="creditcard_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(CieloCreditcardForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();


        if ($form->isSubmitted() && $form->isValid()) {
            $order = new MerchantOrder();
            $order->setCardInfo($form->getData());
            $order->setCustomerName($form->get('holder')->getData());

            $em->persist($order);
            $em->flush();

            $cielo = $this->get('cielo');
            $transaction = $cielo->createSimpleTransaction($order);

            /** @var CieloStatus $status */
            $status = $em->getRepository('AppBundle:CieloStatus')->find($transaction['Payment']['Status']);
            if ($transaction['Payment']['ReturnCode'] == 4) {
                $order->setProofOfSale($transaction['Payment']['ProofOfSale'])
                    ->setAuthCode($transaction['Payment']['AuthorizationCode'])
                    ->setTid($transaction['Payment']['Tid'])
                    ->setPaymentId($transaction['Payment']['PaymentId'])
                    ->setRecurrent($transaction['Payment']['Recurrent'])
                    ->setCapture($transaction['Payment']['Capture'])
                    ->setMessage($transaction['Payment']['ReturnMessage'])
                ;
            }

            $order
                ->setResponse($transaction)
                ->setStatus($status);


            $em->persist($order);
            $em->flush();

            return $this->redirect($this->generateUrl('creditcard_index'));
        }

        /** @var MerchantOrder[] $orders */
        $orders = $em->getRepository('AppBundle:MerchantOrder')
            ->createQueryBuilder('mo')
            ->leftJoin('mo.recurrents', 'rec')
            ->leftJoin('mo.paymentSlips', 'slips')
            ->andWhere('rec.id IS NULL')
            ->andWhere('slips.id IS NULL')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('creditcard/index.html.twig', [
            'form' => $form->createView(),
            'orders' => $orders
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/capture/", name="creditcard_capture")
     * @Method("POST")
     */
    public function captureAction(Request $request)
    {
        $cielo = $this->get('cielo');

        $paymentId = $request->request->get('paymentId');

        try {
            $transaction = $cielo->capture($paymentId);

            $em = $this->getDoctrine()->getManager();

            if (isset($transaction['Status'])) {
                /** @var MerchantOrder $order */
                $order = $em->getRepository('AppBundle:MerchantOrder')
                    ->createQueryBuilder('mo')
                    ->andWhere('mo.paymentId = :payment')
                    ->setParameter('payment', $paymentId)
                    ->getQuery()
                    ->getOneOrNullResult();

                $status = $em->getRepository('AppBundle:CieloStatus')->find($transaction['Status']);

                $order
                    ->setCapture(true)
                    ->setStatus($status);

                $em->persist($order);
                $em->flush();
            }
        } catch (ClientException $e) {
            $this->addFlash('error', "There was an error capturing your payment");
        }


        return $this->redirect($this->generateUrl('creditcard_index'));
    }
}
