<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\RecurrentPayment;
use AppBundle\Form\RecurrentForm;
use AppBundle\Service\Cielo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecurrentController
 * @package AppBundle\Controller
 *
 * @Route("/recurrent")
 */
class RecurrentController extends Controller
{
    /**
     *
     * @Route("/", name="recurrent_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(RecurrentForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $order = new MerchantOrder();
            $order->setCardInfo($form->getData());
            $order->setCustomerName($form->get('holder')->getData());


            $recurrentData = $form->getData();

            $recurrent = new RecurrentPayment();
            $recurrent
                ->setInterval($recurrentData['interval'])
                ->setAuthorizeNow($recurrentData['authorize_now'])
                ->setNextRecurrency($recurrentData['startDate'])
                ->setEndDate($recurrentData['endDate'])
                ->setOrder($order)
            ;

            $em->persist($recurrent);
            $em->persist($order);
            $em->flush();

            $cielo = $this->get('cielo');
            $transaction = $cielo->recurrent($recurrent);


            /** @var CieloStatus $status */
            $status = $em->getRepository('AppBundle:CieloStatus')->find($transaction['Payment']['Status']);

            if (in_array($transaction['Payment']['Status'], [Cielo::AUTHORIZED, Cielo::SCHEDULED])) {

                if (isset($transaction['Payment']['PaymentId'])) {
                    $order->setProofOfSale($transaction['Payment']['ProofOfSale'])
                        ->setAuthCode($transaction['Payment']['AuthorizationCode'])
                        ->setTid($transaction['Payment']['Tid'])
                        ->setMessage($transaction['Payment']['ReturnMessage'])
                        ->setPaymentId($transaction['Payment']['PaymentId']);
                }

                $order
                    ->setRecurrent($transaction['Payment']['Recurrent'])
                    ->setCapture($transaction['Payment']['Capture'])
                    ->setRecurrent($transaction['Payment']['Recurrent'])
                ;

                $nextRecurrency = date_create_from_format('Y-m-d', $transaction['Payment']['RecurrentPayment']['NextRecurrency']);
                $recurrent
                    ->setNextRecurrency($nextRecurrency)
                    ->setRecurrentPaymentId($transaction['Payment']['RecurrentPayment']['RecurrentPaymentId'])
                    ->setResponse($transaction['Payment']['RecurrentPayment']);
            }

            $order
                ->setResponse($transaction)
                ->setStatus($status);

            $em->persist($order);
            $em->flush();

            return $this->redirect($this->generateUrl('recurrent_index'));
        }

        /** @var RecurrentPayment[] $orders */
        $recurrencies = $em->getRepository('AppBundle:RecurrentPayment')
            ->createQueryBuilder('rec')
            ->innerJoin('rec.order', 'mo')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('recurrent/index.html.twig', [
            'form' => $form->createView(),
            'recurrencies' => $recurrencies
        ]);
    }
}
