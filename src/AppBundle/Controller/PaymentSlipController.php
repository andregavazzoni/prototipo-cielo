<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\PaymentSlip;
use AppBundle\Form\CieloCreditcardForm;
use AppBundle\Form\PaymentSlipForm;
use AppBundle\Service\Cielo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaymentSlipController
 * @package AppBundle\Controller
 * @Route("/payment-slip")
 */
class PaymentSlipController extends Controller
{
    /**
     * @Route("/", name="payment_slip_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(PaymentSlipForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();


        if ($form->isSubmitted() && $form->isValid()) {
            $order = new MerchantOrder();
            $order->setCardInfo($form->getData());
            $order->setCustomerName($form->get('customerName')->getData());

            $paymentSlip = new PaymentSlip();
            $paymentSlip->setAmount($form->get('amount')->getData())
                ->setType('Boleto')
                ->setOrder($order)
                ->setProvider('Bradesco')
            ;


            $em->persist($order);
            $em->flush();

            $cielo = $this->get('cielo');
            $transaction = $cielo->createPaymentSlip($paymentSlip);


            if ($transaction['Payment']['Status'] == Cielo::AUTHORIZED) {
                $paymentSlip
                    ->setAddress($transaction['Payment']['Address'])
                    ->setExpirationDate(date_create_from_format('Y-m-d',$transaction['Payment']['ExpirationDate']))
                    ->setPaymentId($transaction['Payment']['PaymentId'])
                    ->setBarCodeNumber($transaction['Payment']['BarCodeNumber'])
                    ->setDigitableLine($transaction['Payment']['DigitableLine'])
                    ->setNumber($transaction['Payment']['BoletoNumber'])
                    ->setUrl($transaction['Payment']['Url'])
                ;
            }
            /** @var CieloStatus $status */
            $status = $em->getRepository('AppBundle:CieloStatus')->find($transaction['Payment']['Status']);

            $paymentSlip->setStatus($status);

            $order
                ->setResponse($transaction)
                ->setStatus($status);


            $em->persist($paymentSlip);
            $em->persist($order);
            $em->flush();

            return $this->redirect($this->generateUrl('payment_slip_index'));
        }

        /** @var MerchantOrder[] $orders */
        $slips = $em->getRepository('AppBundle:PaymentSlip')
            ->createQueryBuilder('ps')
            ->innerJoin('ps.order', 'mo')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('payment_slip/index.html.twig', [
            'form' => $form->createView(),
            'slips' => $slips
        ]);
    }
}
