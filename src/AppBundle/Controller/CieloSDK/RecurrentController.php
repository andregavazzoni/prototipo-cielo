<?php

namespace AppBundle\Controller\CieloSDK;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\RecurrentPayment;
use AppBundle\Form\RecurrentForm;
use AppBundle\Service\Cielo;
use Cielo\API30\Ecommerce\Payment;
use Cielo\API30\Ecommerce\Request\CieloRequestException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RecurrentController
 * @package AppBundle\Controller
 *
 * @Route("/recurrent")
 */
class RecurrentController extends Controller
{
    /**
     *
     * @Route("/", name="sdk_recurrent_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(RecurrentForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $order = new MerchantOrder();
                $order->setCardInfo($form->getData());
                $order->setAmount(10000);
                $order->setCustomerName($form->get('holder')->getData());


                $recurrentData = $form->getData();

                $recurrent = new RecurrentPayment();
                $recurrent
                    ->setInterval($recurrentData['interval'])
                    ->setAuthorizeNow($recurrentData['authorize_now'])
                    ->setNextRecurrency($recurrentData['startDate'])
                    ->setEndDate($recurrentData['endDate'])
                    ->setOrder($order);

                $em->persist($recurrent);
                $em->persist($order);
                $em->flush();

                $cielo = $this->get('cielo_vendor');
                $sale = $cielo->createRecurrent($recurrent);

                /** @var Payment $payment */
                $payment = $sale->getPayment();

                /** @var CieloStatus $status */
                $status = $em->getRepository('AppBundle:CieloStatus')->find($payment->getStatus());

                if (in_array($payment->getStatus(), [Cielo::AUTHORIZED, Cielo::SCHEDULED])) {


                    $order->setProofOfSale($payment->getProofOfSale())
                        ->setAuthCode($payment->getAuthorizationCode())
                        ->setTid($payment->getTid())
                        ->setMessage($payment->getReturnMessage())
                        ->setPaymentId($payment->getPaymentId())
                        ->setRecurrent($payment->getRecurrent())
                        ->setCapture($payment->getCapture());

                    /** @var \Cielo\API30\Ecommerce\RecurrentPayment $recurrentPayment */
                    $recurrentPayment = $payment->getRecurrentPayment();

                    $nextRecurrency = date_create_from_format('Y-m-d', $recurrentPayment->gerNextRecurrency());

                    $recurrent
                        ->setNextRecurrency($nextRecurrency)
                        ->setRecurrentPaymentId($recurrentPayment->getRecurrentPaymentId())
                        ->setResponse($recurrentPayment->jsonSerialize());
                }

                $order
                    ->setResponse($sale->jsonSerialize())
                    ->setStatus($status);

                $em->persist($order);
                $em->flush();
            } catch (CieloRequestException $exception) {
                $this->addFlash('error', $exception->getCieloError()->getMessage());
            }

            return $this->redirect($this->generateUrl('sdk_recurrent_index'));
        }

        /** @var RecurrentPayment[] $orders */
        $recurrencies = $em->getRepository('AppBundle:RecurrentPayment')
            ->createQueryBuilder('rec')
            ->innerJoin('rec.order', 'mo')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('recurrent/index.html.twig', [
            'form' => $form->createView(),
            'recurrencies' => $recurrencies
        ]);
    }
}
