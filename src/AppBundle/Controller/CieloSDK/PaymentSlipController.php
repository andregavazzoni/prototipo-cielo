<?php

namespace AppBundle\Controller\CieloSDK;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\PaymentSlip;
use AppBundle\Form\CieloCreditcardForm;
use AppBundle\Form\PaymentSlipForm;
use AppBundle\Service\Cielo;
use Cielo\API30\Ecommerce\Payment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PaymentSlipController
 * @package AppBundle\Controller
 * @Route("/payment-slip")
 */
class PaymentSlipController extends Controller
{
    /**
     * @Route("/", name="sdk_payment_slip_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(PaymentSlipForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();


        if ($form->isSubmitted() && $form->isValid()) {
            $order = new MerchantOrder();
            $order->setCardInfo($form->getData());
            $order->setCustomerName($form->get('customerName')->getData());

            $paymentSlip = new PaymentSlip();
            $paymentSlip->setAmount($form->get('amount')->getData())
                ->setType('Boleto')
                ->setOrder($order)
                ->setProvider('Bradesco')
            ;


            $em->persist($order);
            $em->flush();

            $cielo = $this->get('cielo_vendor');
            $sale = $cielo->createPaymentSlip($paymentSlip);

            /** @var Payment $payment */
            $payment = $sale->getPayment();

            if ($payment->getStatus() == Cielo::AUTHORIZED) {
                $expirationDate = date_create_from_format('Y-m-d', $payment->getExpirationDate());

                $paymentSlip
                    ->setAddress($payment->getAddress())
                    ->setExpirationDate($expirationDate)
                    ->setPaymentId($payment->getPaymentId())
                    ->setBarCodeNumber($payment->getBarCodeNumber())
                    ->setDigitableLine($payment->getDigitableLine())
                    ->setNumber($payment->getBoletoNumber())
                    ->setUrl($payment->getUrl())
                ;
            }
            /** @var CieloStatus $status */
            $status = $em->getRepository('AppBundle:CieloStatus')->find($payment->getStatus());

            $paymentSlip->setStatus($status);

            $order
                ->setResponse($sale->jsonSerialize())
                ->setStatus($status);


            $em->persist($paymentSlip);
            $em->persist($order);
            $em->flush();

            return $this->redirect($this->generateUrl('sdk_payment_slip_index'));
        }

        /** @var MerchantOrder[] $orders */
        $slips = $em->getRepository('AppBundle:PaymentSlip')
            ->createQueryBuilder('ps')
            ->innerJoin('ps.order', 'mo')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('payment_slip/index.html.twig', [
            'form' => $form->createView(),
            'slips' => $slips
        ]);
    }
}
