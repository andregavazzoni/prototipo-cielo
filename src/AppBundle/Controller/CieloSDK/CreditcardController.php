<?php

namespace AppBundle\Controller\CieloSDK;

use AppBundle\Entity\CieloStatus;
use AppBundle\Entity\MerchantOrder;
use AppBundle\Form\CieloCreditcardForm;
use Cielo\API30\Ecommerce\Payment;
use Cielo\API30\Ecommerce\Request\CieloRequestException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CreditcardController
 * @package AppBundle\Controller
 * @Route("/creditcard")
 */
class CreditcardController extends Controller
{
    /**
     * @Route("/", name="sdk_creditcard_index")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm(CieloCreditcardForm::class);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();


        if ($form->isSubmitted() && $form->isValid()) {
            $order = new MerchantOrder();
            $order->setCardInfo($form->getData());
            $order->setAmount(10000);
            $order->setCustomerName($form->get('holder')->getData());

            $em->persist($order);
            $em->flush();

            $cielo = $this->get('cielo_vendor');

            try {
                $sale = $cielo->createCreditCardTransaction($order);
                /** @var Payment $payment */
                $payment = $sale->getPayment();

                /** @var CieloStatus $status */
                $status = $em->getRepository('AppBundle:CieloStatus')->find($payment->getStatus());

                $order->setProofOfSale($payment->getProofOfSale())
                    ->setAuthCode($payment->getAuthorizationCode())
                    ->setTid($payment->getTid())
                    ->setPaymentId($payment->getPaymentId())
                    ->setRecurrent($payment->getRecurrent())
                    ->setCapture($payment->getCapture())
                    ->setMessage($payment->getReturnMessage())
                    ->setResponse($sale->jsonSerialize(), true)
                    ->setStatus($status);

                $em->persist($order);
                $em->flush();

            } catch (CieloRequestException $exception) {
                $this->addFlash('error', $exception->getCieloError()->getMessage());
            }

            return $this->redirect($this->generateUrl('sdk_creditcard_index'));
        }

        /** @var MerchantOrder[] $orders */
        $orders = $em->getRepository('AppBundle:MerchantOrder')
            ->createQueryBuilder('mo')
            ->leftJoin('mo.recurrents', 'rec')
            ->leftJoin('mo.paymentSlips', 'slp')
            ->andWhere('slp.id IS NULL')
            ->andWhere('rec.id IS NULL')
            ->andWhere('mo.status IS NOT NULL')
            ->orderBy('mo.id', 'DESC')
            ->getQuery()
            ->getResult();

        return $this->render('sdk/creditcard/index.html.twig', [
            'form' => $form->createView(),
            'orders' => $orders
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/capture", name="sdk_creditcard_capture")
     * @Method("POST")
     */
    public function captureAction(Request $request)
    {
        $cielo = $this->get('cielo_vendor');

        try {
            $paymentId = $request->request->get('paymentId');

            $sale = $cielo->getSale($paymentId);
            $capture = $cielo->capture($sale);

            $em = $this->getDoctrine()->getManager();


            /** @var MerchantOrder $order */
            $order = $em->getRepository('AppBundle:MerchantOrder')
                ->createQueryBuilder('mo')
                ->andWhere('mo.paymentId = :payment')
                ->setParameter('payment', $paymentId)
                ->getQuery()
                ->getOneOrNullResult();

            $status = $em->getRepository('AppBundle:CieloStatus')->find($capture->getStatus());

            $order
                ->setCapture(true)
                ->setStatus($status);

            $em->persist($order);
            $em->flush();

        } catch (CieloRequestException $exception) {
            if ($exception->getCieloError() !== null) {
                $this->addFlash('error', $exception->getCieloError()->getMessage());
            } else {
                $this->addFlash('error', "There was an error capturing your payment");
            }
        }


        return $this->redirect($this->generateUrl('sdk_creditcard_index'));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @Route("/void", name="sdk_creditcard_void")
     * @Method("POST")
     */
    public function voidAction(Request $request)
    {
        $cielo = $this->get('cielo_vendor');

        try {
            $paymentId = $request->request->get('paymentId');

            $sale = $cielo->getSale($paymentId);
            $cancel = $cielo->cancel($sale);

            $em = $this->getDoctrine()->getManager();


            /** @var MerchantOrder $order */
            $order = $em->getRepository('AppBundle:MerchantOrder')
                ->createQueryBuilder('mo')
                ->andWhere('mo.paymentId = :payment')
                ->setParameter('payment', $paymentId)
                ->getQuery()
                ->getOneOrNullResult();

            $status = $em->getRepository('AppBundle:CieloStatus')->find($cancel->getStatus());

            $order
                ->setCapture(true)
                ->setStatus($status);

            $em->persist($order);
            $em->flush();

        } catch (CieloRequestException $exception) {
            if ($exception->getCieloError() !== null) {
                $this->addFlash('error', $exception->getCieloError()->getMessage());
            } else {
                $this->addFlash('error', "There was an error.");
            }
        }


        return $this->redirect($this->generateUrl('sdk_creditcard_index'));
    }
}
