<?php

namespace AppBundle\Form;

use AppBundle\Entity\RecurrentPayment;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range as RangeConstraint;

class RecurrentForm extends CieloCreditcardForm
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('authorize_now', ChoiceType::class, [
                'choices' => [
                    'Yes' => 1,
                    'No' => 0,
                ]
            ])/*
            ->add('installments', ChoiceType::class, [
                'choices' => array_combine(range(1, 12), range(1, 12)),
                'constraints' => [
                    new RangeConstraint([
                        'min' => 1,
                        'max' => 12
                    ])
                ]
            ])*/
            ->add('interval', ChoiceType::class, [
                'choices' => array_combine(RecurrentPayment::INTERVAL_ENUM, RecurrentPayment::INTERVAL_ENUM)
            ])
            ->add('startDate', DateType::class, [
                'years' => range(2018, 2023),
                'months' => range(date('m'), 12),
                'days' => range(date('d'), 31)
            ])
            ->add('endDate', DateType::class, [
                'years' => range(2018, 2023),
                'months' => range(date('m') + 1, 12)
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'recurrent';
    }
}
