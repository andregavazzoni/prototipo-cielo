<?php

namespace AppBundle\Form;

use AppBundle\Service\Cielo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CieloCreditcardForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $days = array_map(function ($value) {
            return str_pad($value, 2, 0, STR_PAD_LEFT);
        },range(1, 12));

        $builder
            ->add('holder')
            ->add('card', ChoiceType::class, [
                'choices' => array_combine(array_map(function($value) {
                    return $value['message'].' ('.$value['status'].')';
                }, Cielo::$cards), array_keys(Cielo::$cards))
            ])
            ->add('expiration_month', ChoiceType::class, [
                'choices' => array_combine($days, $days)
            ])
            ->add('expiration_year', ChoiceType::class, [
                'choices' => array_combine(range(2018, 2023), range(2018, 2023))
            ])
            ->add('cvv', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'cielo_creditcard';
    }
}
