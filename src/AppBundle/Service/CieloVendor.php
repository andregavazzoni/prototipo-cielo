<?php

namespace AppBundle\Service;

use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\PaymentSlip;
use AppBundle\Entity\RecurrentPayment;
use Cielo\API30\Ecommerce\CieloEcommerce;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Ecommerce\Payment;
use Cielo\API30\Ecommerce\Sale;
use Cielo\API30\Merchant;

/**
 * Class CieloVendor
 * @package AppBundle\Service
 */
class CieloVendor
{
    /**
     * @var Environment
     */
    private $environment;
    /**
     * @var Merchant
     */
    private $merchant;

    /**
     * Cielo constructor.
     * @param string $id
     * @param string $key
     * @param string $environment
     */
    public function __construct(string $id, string $key, string $environment)
    {
        if ($environment == 'production') {
            $this->environment = Environment::production();
        } else {
            $this->environment = Environment::sandbox();
        }

        $this->merchant = new Merchant($id, $key);
    }

    /**
     * @param MerchantOrder $order
     * @return Sale
     */
    public function createCreditCardTransaction(MerchantOrder $order): Sale
    {
        $creditcardInfo = $order->getCardInfo();

        $sale = new Sale($order->getId());
        $sale->customer($order->getCustomerName());
        $payment = $sale->payment($order->getAmount(), 1);

        $cardnumber = preg_replace('/[.-]/', '', $creditcardInfo['card']);

        $creditcard = $payment
            ->creditCard($cardnumber, 'Visa')
            ->setCardNumber($cardnumber)
            ->setExpirationDate($creditcardInfo['expiration_month'] . "/" . $creditcardInfo['expiration_year'])
            ->setHolder($creditcardInfo['holder'])
            ->setSecurityCode($creditcardInfo['cvv']);
        $payment->setCreditCard($creditcard);
        $sale->setPayment($payment);

        $sale = (new CieloEcommerce($this->merchant, $this->environment))->createSale($sale);

        return $sale;
    }

    /**
     * @param string $paymentId
     * @return Sale
     */
    public function getSale(string $paymentId): Sale
    {
        return (new CieloEcommerce($this->merchant, $this->environment))->getSale($paymentId);
    }

    /**
     * @param Sale $sale
     * @return Payment
     */
    public function capture(Sale $sale): Payment
    {
        /** @var Payment $payment */
        $payment = $sale->getPayment();
        return (new CieloEcommerce($this->merchant, $this->environment))->captureSale($payment->getPaymentId(), $payment->getAmount(), $payment->getServiceTaxAmount());
    }

    /**
     * @param Sale $sale
     * @return Payment
     */
    public function cancel(Sale $sale): Payment
    {
        /** @var Payment $payment */
        $payment = $sale->getPayment();
        return (new CieloEcommerce($this->merchant, $this->environment))->cancelSale($payment->getPaymentId(), $payment->getAmount());
    }

    /**
     * @param PaymentSlip $paymentSlip
     * @return Sale
     */
    public function createPaymentSlip(PaymentSlip $paymentSlip): Sale
    {
        $order = $paymentSlip->getOrder();
        $sale = new Sale($order->getId());
        $sale->customer($order->getCustomerName());

        $sale
            ->payment($paymentSlip->getAmount())
            ->setType(Payment::PAYMENTTYPE_BOLETO)
            ->setProvider($paymentSlip->getProvider())
            ->setExpirationDate(date('Y-m-d', strtotime("+1 days")));

        $sale = (new CieloEcommerce($this->merchant, $this->environment))->createSale($sale);

        return $sale;
    }

    public function createRecurrent(RecurrentPayment $recurrentPayment): Sale
    {
        $order = $recurrentPayment->getOrder();
        $creditcardInfo = $order->getCardInfo();

        $sale = new Sale($order->getId());
        $sale->customer($order->getCustomerName());
        $payment = $sale->payment($order->getAmount(), 1);
        $recurrent = $payment->recurrentPayment($recurrentPayment->getAuthorizeNow());
        $recurrent
            ->setInterval($recurrentPayment->getInterval())
            ->setEndDate($recurrentPayment->getEndDate()->format('Y-m-d'))
        ;

        if (!$recurrentPayment->getAuthorizeNow()) {
            $recurrent->setStartDate($recurrentPayment->getNextRecurrency()->format('Y-m-d'));
        }

        $cardnumber = preg_replace('/[.-]/', '', $creditcardInfo['card']);

        $payment
            ->creditCard($cardnumber, 'Visa')
            ->setCardNumber($cardnumber)
            ->setExpirationDate($creditcardInfo['expiration_month'] . "/" . $creditcardInfo['expiration_year'])
            ->setHolder($creditcardInfo['holder'])
            ->setSecurityCode($creditcardInfo['cvv']);

        $sale = (new CieloEcommerce($this->merchant, $this->environment))->createSale($sale);
        return $sale;
    }
}