<?php

namespace AppBundle\Service;


use AppBundle\Entity\MerchantOrder;
use AppBundle\Entity\PaymentSlip;
use AppBundle\Entity\RecurrentPayment;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class Cielo
 * @package AppBundle\Service
 */
class Cielo
{
    /**
     * @const int
     */
    const NOT_FINISHED = 0;
    /**
     * @const int
     */
    const AUTHORIZED = 1;
    /**
     * @const int
     */
    const PAYMENT_CONFIRMED = 2;
    /**
     * @const int
     */
    const DENIED = 3;
    /**
     * @const int
     */
    const VOIDED = 10;
    /**
     * @const int
     */
    const REFUNDED = 11;
    /**
     * @const int
     */
    const PENDING = 12;
    /**
     * @const int
     */
    const ABORTED = 13;
    /**
     * @const int
     */
    const SCHEDULED = 20;


    /**
     * @var array
     */
    public static $cards = [
        '0000.0000.0000.0001' =>
            [
                'message' => 'Operação realizada com sucesso',
                'code' => 4,
                'status' => 'Autorizado'
            ],
        '0000.0000.0000.0004' =>
            [
                'message' => 'Operação realizada com sucesso',
                'code' => 4,
                'status' => 'Autorizado'
            ],
        '0000.0000.0000.0002' =>
            [
                'message' => 'Não Autorizada',
                'code' => 2,
                'status' => 'Não Autorizado'
            ],
        '0000.0000.0000.0009' =>
            [
                'message' => 'Operation Successful / Time Out',
                'code' => '4/19',
                'status' => 'Autorização Aleatória'
            ],
        '0000.0000.0000.0007' =>
            [
                'message' => 'Cartão Cancelado',
                'code' => 77,
                'status' => 'Não Autorizado'
            ],
        '0000.0000.0000.0008' =>
            [
                'message' => 'Problemas com o Cartão de Crédito',
                'code' => 70,
                'status' => 'Não Autorizado'
            ],
        '0000.0000.0000.0005' =>
            [
                'message' => 'Cartão Bloqueado',
                'code' => 78,
                'status' => 'Não Autorizado'
            ],
        '0000.0000.0000.0003' =>
            [
                'message' => 'Cartão Expirado',
                'code' => 57,
                'status' => 'Não Autorizado'
            ],
        '0000.0000.0000.0006' =>
            [
                'message' => 'Time Out',
                'code' => 99,
                'status' => 'Não Autorizado'
            ]
    ];


    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $endpointRequest;
    /**
     * @var string
     */
    private $endpointQuery;

    /**
     * Cielo constructor.
     * @param string $id
     * @param string $key
     * @param string $endpointRequest
     * @param string $endpointQuery
     */
    public function __construct(string $id, string $key, string $endpointRequest, string $endpointQuery)
    {
        $this->client = new Client([
            'headers' => [
                'MerchantId' => $id,
                'MerchantKey' => $key,
                'Content-Type' => 'application/json'
            ]
        ]);

        $this->endpointRequest = $endpointRequest;
        $this->endpointQuery = $endpointQuery;
    }

    /**
     * @param MerchantOrder $order
     * @return array
     */
    public function createSimpleTransaction(MerchantOrder $order): array
    {
        $creditcard = $order->getCardInfo();

        $data = [
            'MerchantOrderId' => $order->getId(),
            'Customer' => [
                'Name' => $order->getCustomerName()
            ],
            'Payment' => [
                'Type' => 'CreditCard',
                'Amount' => 1000,
                'Installments' => 1,
                'CreditCard' => [
                    'CardNumber' => preg_replace('/[.-]/', '', $creditcard['card']),
                    'Holder' => $creditcard['holder'],
                    'ExpirationDate' => $creditcard['expiration_month'] . "/" . $creditcard['expiration_year'],
                    'SecurityCode' => $creditcard['cvv'],
                    'Brand' => 'Visa'
                ]
            ]
        ];

        try {
            $response = $this->client->post($this->endpointRequest . '/1/sales/', ['body' => json_encode($data)]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param string $paymentId
     * @return array
     */
    public function capture(string $paymentId): array
    {
        $url = sprintf("%s/1/sales/%s/capture", $this->endpointRequest, $paymentId);

        $response = $this->client->put($url);

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    public function recurrent(RecurrentPayment $recurrent)
    {
        $order = $recurrent->getOrder();
        $creditcard = $order->getCardInfo();

        $data = [
            'MerchantOrderId' => $order->getId(),
            'Customer' => [
                'Name' => $order->getCustomerName()
            ],
            'Payment' => [
                'Type' => 'CreditCard',
                'Amount' => 1000,
                'Installments' => 1,
                'CreditCard' => [
                    'CardNumber' => preg_replace('/[.-]/', '', $creditcard['card']),
                    'Holder' => $creditcard['holder'],
                    'ExpirationDate' => $creditcard['expiration_month'] . "/" . $creditcard['expiration_year'],
                    'SecurityCode' => $creditcard['cvv'],
                    'Brand' => 'Visa'
                ],
                'RecurrentPayment' => [
                    'AuthorizeNow' => (bool)$recurrent->getAuthorizeNow(),
                    'EndDate' => $recurrent->getEndDate()->format('Y-m-d'),
                    'Interval' => $recurrent->getInterval()
                ]
            ]
        ];

        if (!(bool)$recurrent->getAuthorizeNow()) {
            $data['RecurrentPayment']['StartDate'] = $recurrent->getNextRecurrency()->format('Y-m-d');
        }

        try {
            $response = $this->client->post($this->endpointRequest . '/1/sales/', ['body' => json_encode($data)]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    public function createPaymentSlip(PaymentSlip $paymentSlip): array
    {
        $order = $paymentSlip->getOrder();

        $data = [
            'MerchantOrderId' => $order->getId(),
            'Customer' => [
                'Name' => $order->getCustomerName()
            ],
            'Payment' => [
                'Type' => $paymentSlip->getType(),
                'Amount' => $paymentSlip->getAmount(),
                'Provider' => $paymentSlip->getProvider()
            ]
        ];

        try {
            $response = $this->client->post($this->endpointRequest . '/1/sales/', ['body' => json_encode($data)]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }
}