<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaymentSlip
 *
 * @ORM\Table(name="payment_slip")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaymentSlipRepository")
 */
class PaymentSlip
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="instructions", type="text", nullable=true)
     */
    private $instructions;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiration_date", type="date")
     */
    private $expirationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="text")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="bar_code_number", type="text")
     */
    private $barCodeNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="digitable_line", type="text")
     */
    private $digitableLine;

    /**
     * @var string
     *
     * @ORM\Column(name="assignor", type="text", nullable=true)
     */
    private $assignor;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var integer
     *
     * @ORM\Column(name="identification", type="integer", nullable=true)
     */
    private $identification;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_id", type="text")
     */
    private $paymentId;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", nullable=true)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", scale=0)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="text", nullable=true)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="text", nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="provider", type="text", nullable=true)
     */
    private $provider;

    /**
     * @var CieloStatus
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CieloStatus")
     */
    private $status;

    /**
     * @var MerchantOrder
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MerchantOrder", inversedBy="paymentSlips")
     */
    private $order;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set instructions
     *
     * @param string $instructions
     *
     * @return PaymentSlip
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Get instructions
     *
     * @return string
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Set expirationDate
     *
     * @param \DateTime $expirationDate
     *
     * @return PaymentSlip
     */
    public function setExpirationDate($expirationDate)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate
     *
     * @return \DateTime
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return PaymentSlip
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return PaymentSlip
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set barCodeNumber
     *
     * @param string $barCodeNumber
     *
     * @return PaymentSlip
     */
    public function setBarCodeNumber($barCodeNumber)
    {
        $this->barCodeNumber = $barCodeNumber;

        return $this;
    }

    /**
     * Get barCodeNumber
     *
     * @return string
     */
    public function getBarCodeNumber()
    {
        return $this->barCodeNumber;
    }

    /**
     * Set digitableLine
     *
     * @param string $digitableLine
     *
     * @return PaymentSlip
     */
    public function setDigitableLine($digitableLine)
    {
        $this->digitableLine = $digitableLine;

        return $this;
    }

    /**
     * Get digitableLine
     *
     * @return string
     */
    public function getDigitableLine()
    {
        return $this->digitableLine;
    }

    /**
     * Set assignor
     *
     * @param string $assignor
     *
     * @return PaymentSlip
     */
    public function setAssignor($assignor)
    {
        $this->assignor = $assignor;

        return $this;
    }

    /**
     * Get assignor
     *
     * @return string
     */
    public function getAssignor()
    {
        return $this->assignor;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return PaymentSlip
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set identification
     *
     * @param integer $identification
     *
     * @return PaymentSlip
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;

        return $this;
    }

    /**
     * Get identification
     *
     * @return integer
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     *
     * @return PaymentSlip
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PaymentSlip
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return PaymentSlip
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return PaymentSlip
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return PaymentSlip
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set provider
     *
     * @param string $provider
     *
     * @return PaymentSlip
     */
    public function setProvider($provider)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return string
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set status
     *
     * @param CieloStatus $status
     *
     * @return PaymentSlip
     */
    public function setStatus(CieloStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return CieloStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set order
     *
     * @param \AppBundle\Entity\MerchantOrder $order
     *
     * @return PaymentSlip
     */
    public function setOrder(\AppBundle\Entity\MerchantOrder $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \AppBundle\Entity\MerchantOrder
     */
    public function getOrder()
    {
        return $this->order;
    }
}
