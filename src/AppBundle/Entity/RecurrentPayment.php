<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecurrentPayment
 *
 * @ORM\Table(name="recurrent_payment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecurrentPaymentRepository")
 */
class RecurrentPayment
{
    const INTERVAL_ENUM = [
        'Monthly',
        'Bimonthly',
        'Quarterly',
        'SemiAnnual',
        'Annual',
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="recurrent_payment_id", type="string", length=255, nullable=true)
     */
    private $recurrentPaymentId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="next_recurrency", type="date")
     */
    private $nextRecurrency;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="interval", type="string", length=255)
     */
    private $interval;

    /**
     * @var bool
     *
     * @ORM\Column(name="authorize_now", type="boolean")
     */
    private $authorizeNow;

    /**
     * @var array
     *
     * @ORM\Column(name="response", type="array", nullable=true)
     */
    private $response;

    /**
     * @var MerchantOrder
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MerchantOrder", inversedBy="recurrents")
     */
    private $order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get recurrentPaymentId
     *
     * @return string
     */
    public function getRecurrentPaymentId()
    {
        return $this->recurrentPaymentId;
    }

    /**
     * Set recurrentPaymentId
     *
     * @param string $recurrentPaymentId
     *
     * @return RecurrentPayment
     */
    public function setRecurrentPaymentId($recurrentPaymentId)
    {
        $this->recurrentPaymentId = $recurrentPaymentId;

        return $this;
    }

    /**
     * Get nextRecurrency
     *
     * @return \DateTime
     */
    public function getNextRecurrency()
    {
        return $this->nextRecurrency;
    }

    /**
     * Set nextRecurrency
     *
     * @param \DateTime $nextRecurrency
     *
     * @return RecurrentPayment
     */
    public function setNextRecurrency($nextRecurrency)
    {
        $this->nextRecurrency = $nextRecurrency;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return RecurrentPayment
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get interval
     *
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Set interval
     *
     * @param string $interval
     *
     * @return RecurrentPayment
     */
    public function setInterval($interval)
    {
        if (!in_array($interval, self::INTERVAL_ENUM)) {
            throw new \RuntimeException("Interval should be ".implode(', ', self::INTERVAL_ENUM));
        }

        $this->interval = $interval;

        return $this;
    }

    /**
     * Get authorizeNow
     *
     * @return boolean
     */
    public function getAuthorizeNow()
    {
        return $this->authorizeNow;
    }

    /**
     * Set authorizeNow
     *
     * @param boolean $authorizeNow
     *
     * @return RecurrentPayment
     */
    public function setAuthorizeNow($authorizeNow)
    {
        $this->authorizeNow = $authorizeNow;

        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set response
     *
     * @param array $response
     *
     * @return RecurrentPayment
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get order
     *
     * @return MerchantOrder
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param MerchantOrder $order
     *
     * @return RecurrentPayment
     */
    public function setOrder(MerchantOrder $order = null)
    {
        $this->order = $order;

        return $this;
    }
}
