<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * MerchantOrder
 *
 * @ORM\Table(name="merchant_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MerchantOrderRepository")
 */
class MerchantOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_name", type="string", length=255)
     */
    private $customerName;

    /**
     * @var string
     *
     * @ORM\Column(name="proof_of_sale", type="string", length=255, nullable=true)
     */
    private $proofOfSale;

    /**
     * @var string
     *
     * @ORM\Column(name="tid", type="string", length=255, nullable=true)
     */
    private $tid;

    /**
     * @var string
     *
     * @ORM\Column(name="auth_code", type="string", length=255, nullable=true)
     */
    private $authCode;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_id", type="string", length=255, nullable=true)
     */
    private $paymentId;

    /**
     * @var array
     *
     * @ORM\Column(name="card_info", type="array")
     */
    private $cardInfo;

    /**
     * @var bool
     *
     * @ORM\Column(name="capture", type="boolean", nullable=true)
     */
    private $capture;

    /**
     * @var bool
     *
     * @ORM\Column(name="recurrent", type="boolean", nullable=true)
     */
    private $recurrent;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text", nullable=true)
     */
    private $message;

    /**
     * @var  array
     *
     * @ORM\Column(name="response", type="array", nullable=true)
     */
    private $response;

    /**
     * @var CieloStatus
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CieloStatus")
     */
    private $status;

    /**
     * @var RecurrentPayment[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RecurrentPayment", mappedBy="order")
     */
    private $recurrents;

    /**
     * @var PaymentSlip[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PaymentSlip", mappedBy="order")
     */
    private $paymentSlips;

    /**
     * @var integer
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return MerchantOrder
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set proofOfSale
     *
     * @param string $proofOfSale
     *
     * @return MerchantOrder
     */
    public function setProofOfSale($proofOfSale)
    {
        $this->proofOfSale = $proofOfSale;

        return $this;
    }

    /**
     * Get proofOfSale
     *
     * @return string
     */
    public function getProofOfSale()
    {
        return $this->proofOfSale;
    }

    /**
     * Set tid
     *
     * @param string $tid
     *
     * @return MerchantOrder
     */
    public function setTid($tid)
    {
        $this->tid = $tid;

        return $this;
    }

    /**
     * Get tid
     *
     * @return string
     */
    public function getTid()
    {
        return $this->tid;
    }

    /**
     * Set authCode
     *
     * @param string $authCode
     *
     * @return MerchantOrder
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;

        return $this;
    }

    /**
     * Get authCode
     *
     * @return string
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * Set paymentId
     *
     * @param string $paymentId
     *
     * @return MerchantOrder
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;

        return $this;
    }

    /**
     * Get paymentId
     *
     * @return string
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * Set cardInfo
     *
     * @param array $cardInfo
     *
     * @return MerchantOrder
     */
    public function setCardInfo($cardInfo)
    {
        $this->cardInfo = $cardInfo;

        return $this;
    }

    /**
     * Get cardInfo
     *
     * @return array
     */
    public function getCardInfo()
    {
        return $this->cardInfo;
    }

    /**
     * Set capture
     *
     * @param boolean $capture
     *
     * @return MerchantOrder
     */
    public function setCapture($capture)
    {
        $this->capture = $capture;

        return $this;
    }

    /**
     * Get capture
     *
     * @return boolean
     */
    public function getCapture()
    {
        return $this->capture;
    }

    /**
     * Set recurrent
     *
     * @param boolean $recurrent
     *
     * @return MerchantOrder
     */
    public function setRecurrent($recurrent)
    {
        $this->recurrent = $recurrent;

        return $this;
    }

    /**
     * Get recurrent
     *
     * @return boolean
     */
    public function getRecurrent()
    {
        return $this->recurrent;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return MerchantOrder
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set response
     *
     * @param array $response
     *
     * @return MerchantOrder
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return array
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set status
     *
     * @param CieloStatus $status
     *
     * @return MerchantOrder
     */
    public function setStatus(CieloStatus $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return CieloStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recurrents = new ArrayCollection();
        $this->paymentSlips = new ArrayCollection();
    }

    /**
     * Add recurrent
     *
     * @param RecurrentPayment $recurrent
     *
     * @return MerchantOrder
     */
    public function addRecurrent(RecurrentPayment $recurrent)
    {
        $this->recurrents[] = $recurrent;

        return $this;
    }

    /**
     * Remove recurrent
     *
     * @param RecurrentPayment $recurrent
     */
    public function removeRecurrent(RecurrentPayment $recurrent)
    {
        $this->recurrents->removeElement($recurrent);
    }

    /**
     * Get recurrents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecurrents()
    {
        return $this->recurrents;
    }

    /**
     * Add paymentSlip
     *
     * @param PaymentSlip $paymentSlip
     *
     * @return MerchantOrder
     */
    public function addPaymentSlip(PaymentSlip $paymentSlip)
    {
        $this->paymentSlips[] = $paymentSlip;

        return $this;
    }

    /**
     * Remove paymentSlip
     *
     * @param PaymentSlip $paymentSlip
     */
    public function removePaymentSlip(PaymentSlip $paymentSlip)
    {
        $this->paymentSlips->removeElement($paymentSlip);
    }

    /**
     * Get paymentSlips
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentSlips()
    {
        return $this->paymentSlips;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return MerchantOrder
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
