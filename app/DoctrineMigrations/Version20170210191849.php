<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170210191849 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, status, card_info, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, customer_name VARCHAR(255) NOT NULL COLLATE BINARY, proof_of_sale VARCHAR(255) DEFAULT NULL COLLATE BINARY, tid VARCHAR(255) DEFAULT NULL COLLATE BINARY, auth_code VARCHAR(255) DEFAULT NULL COLLATE BINARY, payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL COLLATE BINARY, card_info CLOB NOT NULL, response CLOB DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_62BA49DF6BF700BD FOREIGN KEY (status_id) REFERENCES cielo_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO merchant_order (id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, status_id, card_info, response) SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, status, card_info, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
        $this->addSql('CREATE INDEX IDX_62BA49DF6BF700BD ON merchant_order (status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_62BA49DF6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, customer_name VARCHAR(255) NOT NULL, proof_of_sale VARCHAR(255) DEFAULT NULL, tid VARCHAR(255) DEFAULT NULL, auth_code VARCHAR(255) DEFAULT NULL, payment_id VARCHAR(255) DEFAULT NULL, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL, status INTEGER DEFAULT NULL, card_info CLOB NOT NULL COLLATE BINARY, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO merchant_order (id, status, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response) SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
    }
}
