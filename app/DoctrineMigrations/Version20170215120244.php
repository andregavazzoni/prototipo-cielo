<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170215120244 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_DCBAD48C8D9F6D38');
        $this->addSql('DROP INDEX IDX_DCBAD48C6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__payment_slip AS SELECT id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider FROM payment_slip');
        $this->addSql('DROP TABLE payment_slip');
        $this->addSql('CREATE TABLE payment_slip (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, order_id INTEGER DEFAULT NULL, instructions CLOB DEFAULT NULL COLLATE BINARY, expiration_date DATE NOT NULL, url CLOB NOT NULL COLLATE BINARY, number CLOB NOT NULL COLLATE BINARY, bar_code_number CLOB NOT NULL COLLATE BINARY, digitable_line CLOB NOT NULL COLLATE BINARY, assignor CLOB DEFAULT NULL COLLATE BINARY, address CLOB DEFAULT NULL COLLATE BINARY, identification INTEGER DEFAULT NULL, payment_id CLOB NOT NULL COLLATE BINARY, type CLOB DEFAULT NULL COLLATE BINARY, amount INTEGER NOT NULL, currency CLOB DEFAULT NULL COLLATE BINARY, country CLOB DEFAULT NULL COLLATE BINARY, provider CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_DCBAD48C6BF700BD FOREIGN KEY (status_id) REFERENCES cielo_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_DCBAD48C8D9F6D38 FOREIGN KEY (order_id) REFERENCES merchant_order (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO payment_slip (id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider) SELECT id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider FROM __temp__payment_slip');
        $this->addSql('DROP TABLE __temp__payment_slip');
        $this->addSql('CREATE INDEX IDX_DCBAD48C8D9F6D38 ON payment_slip (order_id)');
        $this->addSql('CREATE INDEX IDX_DCBAD48C6BF700BD ON payment_slip (status_id)');
        $this->addSql('DROP INDEX IDX_2A89FDF78D9F6D38');
        $this->addSql('CREATE TEMPORARY TABLE __temp__recurrent_payment AS SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM recurrent_payment');
        $this->addSql('DROP TABLE recurrent_payment');
        $this->addSql('CREATE TABLE recurrent_payment (id INTEGER NOT NULL, order_id INTEGER DEFAULT NULL, recurrent_payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, next_recurrency DATE NOT NULL, end_date DATE NOT NULL, interval VARCHAR(255) NOT NULL COLLATE BINARY, authorize_now BOOLEAN NOT NULL, response CLOB DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_2A89FDF78D9F6D38 FOREIGN KEY (order_id) REFERENCES merchant_order (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO recurrent_payment (id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response) SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM __temp__recurrent_payment');
        $this->addSql('DROP TABLE __temp__recurrent_payment');
        $this->addSql('CREATE INDEX IDX_2A89FDF78D9F6D38 ON recurrent_payment (order_id)');
        $this->addSql('DROP INDEX IDX_62BA49DF6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, customer_name VARCHAR(255) NOT NULL COLLATE BINARY, proof_of_sale VARCHAR(255) DEFAULT NULL COLLATE BINARY, tid VARCHAR(255) DEFAULT NULL COLLATE BINARY, auth_code VARCHAR(255) DEFAULT NULL COLLATE BINARY, payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL COLLATE BINARY, card_info CLOB NOT NULL, response CLOB DEFAULT NULL, amount INTEGER DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_62BA49DF6BF700BD FOREIGN KEY (status_id) REFERENCES cielo_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO merchant_order (id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response) SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
        $this->addSql('CREATE INDEX IDX_62BA49DF6BF700BD ON merchant_order (status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_62BA49DF6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, customer_name VARCHAR(255) NOT NULL, proof_of_sale VARCHAR(255) DEFAULT NULL, tid VARCHAR(255) DEFAULT NULL, auth_code VARCHAR(255) DEFAULT NULL, payment_id VARCHAR(255) DEFAULT NULL, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL, card_info CLOB NOT NULL COLLATE BINARY, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO merchant_order (id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response) SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
        $this->addSql('CREATE INDEX IDX_62BA49DF6BF700BD ON merchant_order (status_id)');
        $this->addSql('DROP INDEX IDX_DCBAD48C6BF700BD');
        $this->addSql('DROP INDEX IDX_DCBAD48C8D9F6D38');
        $this->addSql('CREATE TEMPORARY TABLE __temp__payment_slip AS SELECT id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider FROM payment_slip');
        $this->addSql('DROP TABLE payment_slip');
        $this->addSql('CREATE TABLE payment_slip (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, order_id INTEGER DEFAULT NULL, instructions CLOB DEFAULT NULL, expiration_date DATE NOT NULL, url CLOB NOT NULL, number CLOB NOT NULL, bar_code_number CLOB NOT NULL, digitable_line CLOB NOT NULL, assignor CLOB DEFAULT NULL, address CLOB DEFAULT NULL, identification INTEGER DEFAULT NULL, payment_id CLOB NOT NULL, type CLOB DEFAULT NULL, amount INTEGER NOT NULL, currency CLOB DEFAULT NULL, country CLOB DEFAULT NULL, provider CLOB DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO payment_slip (id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider) SELECT id, status_id, order_id, instructions, expiration_date, url, number, bar_code_number, digitable_line, assignor, address, identification, payment_id, type, amount, currency, country, provider FROM __temp__payment_slip');
        $this->addSql('DROP TABLE __temp__payment_slip');
        $this->addSql('CREATE INDEX IDX_DCBAD48C6BF700BD ON payment_slip (status_id)');
        $this->addSql('CREATE INDEX IDX_DCBAD48C8D9F6D38 ON payment_slip (order_id)');
        $this->addSql('DROP INDEX IDX_2A89FDF78D9F6D38');
        $this->addSql('CREATE TEMPORARY TABLE __temp__recurrent_payment AS SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM recurrent_payment');
        $this->addSql('DROP TABLE recurrent_payment');
        $this->addSql('CREATE TABLE recurrent_payment (id INTEGER NOT NULL, order_id INTEGER DEFAULT NULL, recurrent_payment_id VARCHAR(255) DEFAULT NULL, next_recurrency DATE NOT NULL, end_date DATE NOT NULL, interval VARCHAR(255) NOT NULL, authorize_now BOOLEAN NOT NULL, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO recurrent_payment (id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response) SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM __temp__recurrent_payment');
        $this->addSql('DROP TABLE __temp__recurrent_payment');
        $this->addSql('CREATE INDEX IDX_2A89FDF78D9F6D38 ON recurrent_payment (order_id)');
    }
}
