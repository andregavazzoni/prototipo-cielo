<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170213125252 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_2A89FDF78D9F6D38');
        $this->addSql('CREATE TEMPORARY TABLE __temp__recurrent_payment AS SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM recurrent_payment');
        $this->addSql('DROP TABLE recurrent_payment');
        $this->addSql('CREATE TABLE recurrent_payment (id INTEGER NOT NULL, order_id INTEGER DEFAULT NULL, recurrent_payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, next_recurrency DATE NOT NULL, end_date DATE NOT NULL, interval VARCHAR(255) NOT NULL COLLATE BINARY, authorize_now BOOLEAN NOT NULL, response CLOB DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_2A89FDF78D9F6D38 FOREIGN KEY (order_id) REFERENCES merchant_order (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO recurrent_payment (id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response) SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM __temp__recurrent_payment');
        $this->addSql('DROP TABLE __temp__recurrent_payment');
        $this->addSql('CREATE INDEX IDX_2A89FDF78D9F6D38 ON recurrent_payment (order_id)');
        $this->addSql('DROP INDEX IDX_62BA49DF6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, customer_name VARCHAR(255) NOT NULL COLLATE BINARY, proof_of_sale VARCHAR(255) DEFAULT NULL COLLATE BINARY, tid VARCHAR(255) DEFAULT NULL COLLATE BINARY, auth_code VARCHAR(255) DEFAULT NULL COLLATE BINARY, payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL COLLATE BINARY, card_info CLOB NOT NULL, response CLOB DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_62BA49DF6BF700BD FOREIGN KEY (status_id) REFERENCES cielo_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO merchant_order (id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response) SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
        $this->addSql('CREATE INDEX IDX_62BA49DF6BF700BD ON merchant_order (status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_62BA49DF6BF700BD');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, status_id INTEGER DEFAULT NULL, customer_name VARCHAR(255) NOT NULL, proof_of_sale VARCHAR(255) DEFAULT NULL, tid VARCHAR(255) DEFAULT NULL, auth_code VARCHAR(255) DEFAULT NULL, payment_id VARCHAR(255) DEFAULT NULL, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL, card_info CLOB NOT NULL COLLATE BINARY, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO merchant_order (id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response) SELECT id, status_id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
        $this->addSql('CREATE INDEX IDX_62BA49DF6BF700BD ON merchant_order (status_id)');
        $this->addSql('DROP INDEX IDX_2A89FDF78D9F6D38');
        $this->addSql('CREATE TEMPORARY TABLE __temp__recurrent_payment AS SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM recurrent_payment');
        $this->addSql('DROP TABLE recurrent_payment');
        $this->addSql('CREATE TABLE recurrent_payment (id INTEGER NOT NULL, order_id INTEGER DEFAULT NULL, recurrent_payment_id VARCHAR(255) DEFAULT NULL, next_recurrency DATE NOT NULL, end_date DATE NOT NULL, interval VARCHAR(255) NOT NULL, authorize_now BOOLEAN NOT NULL, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO recurrent_payment (id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response) SELECT id, order_id, recurrent_payment_id, next_recurrency, end_date, interval, authorize_now, response FROM __temp__recurrent_payment');
        $this->addSql('DROP TABLE __temp__recurrent_payment');
        $this->addSql('CREATE INDEX IDX_2A89FDF78D9F6D38 ON recurrent_payment (order_id)');
    }
}
