<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170210190945 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE cielo_status (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response, status FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, customer_name VARCHAR(255) NOT NULL COLLATE BINARY, proof_of_sale VARCHAR(255) DEFAULT NULL COLLATE BINARY, tid VARCHAR(255) DEFAULT NULL COLLATE BINARY, auth_code VARCHAR(255) DEFAULT NULL COLLATE BINARY, payment_id VARCHAR(255) DEFAULT NULL COLLATE BINARY, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL COLLATE BINARY, status INTEGER DEFAULT NULL, card_info CLOB NOT NULL, response CLOB DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO merchant_order (id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response, status) SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, capture, recurrent, message, card_info, response, status FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');

        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (0, 'Not Finished')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (1, 'Authorized')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (2, 'Payment Confirmed')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (3, 'Denied')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (10, 'Voided')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (11, 'Refunded')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (12, 'Pending')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (13, 'Aborted')");
        $this->addSql("INSERT INTO cielo_status(id, name) VALUES (20, 'Scheduled')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE cielo_status');
        $this->addSql('CREATE TEMPORARY TABLE __temp__merchant_order AS SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response, status FROM merchant_order');
        $this->addSql('DROP TABLE merchant_order');
        $this->addSql('CREATE TABLE merchant_order (id INTEGER NOT NULL, customer_name VARCHAR(255) NOT NULL, proof_of_sale VARCHAR(255) DEFAULT NULL, tid VARCHAR(255) DEFAULT NULL, auth_code VARCHAR(255) DEFAULT NULL, payment_id VARCHAR(255) DEFAULT NULL, capture BOOLEAN DEFAULT NULL, recurrent BOOLEAN DEFAULT NULL, message CLOB DEFAULT NULL, status INTEGER DEFAULT NULL, card_info CLOB NOT NULL COLLATE BINARY, response CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO merchant_order (id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response, status) SELECT id, customer_name, proof_of_sale, tid, auth_code, payment_id, card_info, capture, recurrent, message, response, status FROM __temp__merchant_order');
        $this->addSql('DROP TABLE __temp__merchant_order');
    }
}
